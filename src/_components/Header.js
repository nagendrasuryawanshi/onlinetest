import React from 'react';
import {Link} from 'react-router-dom'
import {Navbar, NavItem} from 'react-materialize';

class Header extends React.Component{
  render(){
    return (
    <div>
         <nav class="navbar navbar-inverse" id="colorFullHeader"> 
        <NavItem><Link to="/Home" activeClassName="active">Home</Link></NavItem>
        <NavItem><Link to="/Sign-In" activeClassName="active">Sign In</Link></NavItem>
        <NavItem><Link to="/Register" activeClassName="active">Register</Link></NavItem>
      </nav>
      {this.props.children}
    </div>
    )
  }
}
export default Header