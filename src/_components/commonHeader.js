import React from 'react';
// import {FormControl,Nav,Form,Button } from 'react';
// import {FormControl,Nav, Form,FormGroup, ControlLabel, HelpBlock, Checkbox, Radio, Button} from 'react-bootstrap';
//import { Link} from 'react-router-dom';
import { connect } from 'react-redux';
// import Navbar from 'react-bootstrap/Navbar'
import { userActions } from '../_actions';
//import { history } from '../_helpers';

class Commonhearder extends React.Component {
    constructor(props) {
        super(props);

        this.logOut = this.logOut.bind(this);
    }
    logOut(){
       
        const { dispatch } = this.props;
        console.log("dispatch",dispatch)
       // if(dispatch)
        dispatch(userActions.logout());
    }
    componentDidMount() {
       // this.props.dispatch(userActions.getAll());
    }
componentWillReceiveProps(nextprops){
    console.log("Auth",nextprops.authentication)
    if(!nextprops.authentication ){
       // if(!nextprops.authentication.loggedIn)
      //  history.push('/login')
    }
    
}
    handleDeleteUser(id) {
        return (e) => this.props.dispatch(userActions.delete(id));
    }

    render() {
        const { user, users,match } = this.props;
       console.log("user",user)
      var path= window.location.pathname;
      console.log("path",path)
        return (
            <div>
            { user && user.token &&  <nav className="navbar navbar-inverse" id="colorFullHeader">   
          {/* navbar-default */}
          <div className="container-fluid">
                    <div className="navbar-header">
                    <a className="navbar-brand" href="#">Online Test</a>
                    </div>
                    <ul className="nav navbar-nav">
                    <li className={path && path.toLowerCase()=="/" ?"active":""}><a href="/">Dashboard</a></li>
                    <li className= {path && path.toLowerCase()=="/profile" ?"active":""}> <a  href='../Profile'  >Profile</a></li>
                    <li  className={path && path.toLowerCase()=="/changepassword" ?"active":""}> <a href='../changepassword'>Change Password</a></li>
                    
                    </ul>
                    {/* <ul class="nav navbar-nav navbar-right">
                    <li><a href="#"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
                    <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
                    </ul> */}
                    <ul className="nav navbar-nav navbar-right">
                        <li><a href="#"><span className="glyphicon glyphicon-user"></span> {user.firstName +" "+user.lastName}</a></li>
                        <li><a href="#" onClick={this.logOut}><span className="glyphicon glyphicon-log-in"></span> Logout</a></li>
                    </ul>
                </div>
            </nav>}
                        </div>
        );
    }
}
//export default Commonhearder;



function mapStateToProps(state) {
    const { users, authentication } = state;
    const { user } = authentication;
    return {
        user,
        users
    };
}
export default connect( mapStateToProps)(Commonhearder)

// const connectedCommonhearder = connect(mapStateToProps)(Commonhearder);
//  export { connectedCommonhearder as Commonhearder };