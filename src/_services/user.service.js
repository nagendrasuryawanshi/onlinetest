import { authHeader } from '../_helpers';

export const userService = {
    login,
    logout,
    register,
    getAll,
    getById,
    update,
    delete: _delete
};

function login(username, password) {
    localStorage.removeItem("token");
    //console.log("username",username, "password",password)
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json',
        
        'Authorization': 'Basic ' + btoa(username + ":" + password) },
        body: JSON.stringify({ username, password })
    };
    var token="";
    return fetch(`http://test.oles.enastee.org/login`, requestOptions)
        .then(function(response) {
             token = response.headers.get('Token');
           
                console.log("token",token);
           
            // throw new TypeError("Oops, we haven't got JSON!");
                }
            )
        .then(user => {
             console.log("user",user);

             if(!user){
                 user={firstName:'Nagendra',lastName:'Suryawanshi'}
             }
             user['token']=token
            //localStorage.setItem("user",JSON.stringify(user));
            // store user details and jwt token in local storage to keep user logged in between page refreshes
            if(token && token!='')
            localStorage.setItem('user', JSON.stringify(user));

            return user;
        })
}

function logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('user');
}

function getAll() {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return fetch(`http://test.oles.enastee.org/v1/Products/all`, requestOptions).then(handleResponse);
}

function getById(id) {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return fetch(`/users/${id}`, requestOptions).then(handleResponse);
}

function register(user) {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(user)
    };

    return fetch(`/users/register`, requestOptions).then(handleResponse);
}

function update(user) {
    const requestOptions = {
        method: 'PUT',
        headers: { ...authHeader(), 'Content-Type': 'application/json' },
        body: JSON.stringify(user)
    };

    return fetch(`/users/${user.id}`, requestOptions).then(handleResponse);;
}

// prefixed function name with underscore because delete is a reserved word in javascript
function _delete(id) {
    const requestOptions = {
        method: 'DELETE',
        headers: authHeader()
    };

    return fetch(`/users/${id}`, requestOptions).then(handleResponse);
}

function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 401) {
                // auto logout if 401 response returned from api
                logout();
                window.location.reload(true);
            }

            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }

        return data;
    });
}