import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { userActions } from '../_actions';
import { compose } from 'redux';

class ChangePasswordPage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            password: {              
                oldpassword: '',
                newpassword: ''
            },
            submitted: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        const { name, value } = event.target;
        const { password } = this.state;
        this.setState({
            password: {
                ...password,
                [name]: value
            }
        });
    }

    handleSubmit(event) {
        event.preventDefault();

        this.setState({ submitted: true });
        const { password } = this.state;
        const { dispatch } = this.props;
        console.log("password",password,this.state.passord)
        if ( password && password.oldpassword && password.newpassword ) {
            dispatch(userActions.register(password));
        }
    }

    render() {
        const { registering  } = this.props;
        const { password, submitted } = this.state;
        console.log("Password",password,this.state.password)
        return (
            <div className="col-md-6 col-md-offset-3">
                <h2>Change Password</h2>
                <form name="form" onSubmit={this.handleSubmit}>
                   
                    <div className={'form-group' + (submitted && !password.oldpassword ? ' has-error' : '')}>
                        <label htmlFor="oldpassword">Old Password</label>
                        <input type="password" className="form-control" name="oldpassword" value={password && password.oldpassword? password.oldpassword:""} onChange={this.handleChange} />
                        {submitted &&   !password.oldpassword &&
                            <div className="help-block">Old password is required</div>
                        }
                    </div>
                    <div className={'form-group' + (submitted && !password.newpassword ? ' has-error' : '')}>
                        <label htmlFor="newpassword">New Password</label>
                        <input type="password" className="form-control" name="newpassword" value={password && password.newpassword} onChange={this.handleChange} />
                        {submitted && !password.newpassword &&
                            <div className="help-block">New password is required</div>
                        }
                    </div>
                    <div className={'form-group' + (submitted && !password.newpassword ? ' has-error' : '')}>
                        <label htmlFor="newpassword">Confirm Password</label>
                        <input type="password" className="form-control" name="newpassword" value={password && password.newpassword} onChange={this.handleChange} />
                        {submitted && !password.newpassword &&
                            <div className="help-block">Confirm password is required</div>
                        }
                    </div>
                    <div className="form-group">
                        <button className="btn btn-primary">Register</button>
                        {registering && 
                            <img src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==" />
                        }
                        
                        <a href="/" className="btn btn-link">Cancel</a>
                        {/* <Link to="/" className="btn btn-link">Cancel</Link> */}
                    </div>
                </form>
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { registering } = state.registration;
    return {
        registering
    };
}

const connectedChangePasswordPage = connect(mapStateToProps)(ChangePasswordPage);
export { connectedChangePasswordPage as ChangePasswordPage };